package com.mapdistanceCal;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/** This class is used for Getting location form GPS */

public class LocationTracker implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

	private LocationRequest mLocationRequest;
	private GoogleApiClient mLocationClient;
	private Context context;
	boolean mUpdatesRequested = false;
	private LocationResult locationResult;

	public LocationTracker(Context context, LocationResult locationResult) {
		super();
		this.context = context;
		this.locationResult = locationResult;

	}


	public void onUpdateLocation() {

		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(1000);
		mLocationRequest.setFastestInterval(1000);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


		mLocationClient = new GoogleApiClient.Builder(context, LocationTracker.this, LocationTracker.this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();


		if (mLocationClient.isConnected()) {
			startUpdates();
		} else {
			mLocationClient.connect();
		}

	}


	@Override
	public void onConnectionFailed(ConnectionResult result) {


	}

	@Override
	public void onConnected(Bundle connectionHint) {
		startUpdates();
	}


	@Override
	public void onLocationChanged(Location location) {

		if (location != null) {

			if (location.getLatitude() != 0 && location.getLongitude() != 0) {

				location.setLatitude(location.getLatitude());
				location.setLongitude(location.getLongitude());
					/*location.setLatitude(37.8756165);
					location.setLongitude(-122.2699811);*/

				locationResult.gotLocation(location);

				if (mLocationClient != null && mLocationClient.isConnected()) {
					stopPeriodicUpdates();
					mLocationClient.disconnect();
				}
			}
		}

	}

	/**
	 * In response to a request to start updates, send a request
	 * to Location Services
	 */
	private void startPeriodicUpdates() {

		if(checkPermission()) {
			if (mLocationClient.isConnected())
			LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, LocationTracker.this);
		}
	}

	/**
	 * In response to a request to stop updates, send a request to
	 * Location Services
	 */
	private void stopPeriodicUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient,LocationTracker.this);

	}


	public void startUpdates() {
		mUpdatesRequested = true;
		startPeriodicUpdates();
	}


	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub

	}

	private boolean checkPermission(){
		int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
		if (result == PackageManager.PERMISSION_GRANTED){
			return true;
		} else {
			return false;

		}
	}

}
