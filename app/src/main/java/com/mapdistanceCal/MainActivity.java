package com.mapdistanceCal;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, LocationResult {

    private static final String MAP_SHARED = "map_shared";
    private static final long SCHEDULE_TIME = 5 * 1000;
    private SupportMapFragment mapView;
    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private LatLng currentLatLng;
    private boolean checkGPS;
    private boolean checkNetwork;
    private LocationTracker locationTracker;
    private TextView textView,textView1,textView2;
    private String LATITUDE="latitude",LONGITUDE="longitude";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mapView = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        currentLatLng =new LatLng(0,0);
        mapView.getMapAsync(this);
        textView = findViewById(R.id.textView);
        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getSharedPreferences(MAP_SHARED, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(LATITUDE,""+currentLatLng.latitude);
                editor.putString(LONGITUDE,""+currentLatLng.longitude);
                editor.apply();
                start();
                Toast.makeText(MainActivity.this,"Calculation is started.",Toast.LENGTH_SHORT).show();
            }
        });
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Calculation is stoped.",Toast.LENGTH_SHORT).show();
                stop();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        }else {
            // get GPS status
            checkGPS = mLocationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // get network provider status
            checkNetwork = mLocationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!checkGPS && !checkNetwork){
                Toast.makeText(this,"Please enable the GPS.",Toast.LENGTH_SHORT).show();
            }else {

               locationTracker =new LocationTracker(this,this);
                locationTracker.onUpdateLocation();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }


    private String distance(double lat1, double lon1, double lat2, double lon2) {
        Log.e("Source: ",""+lat1+","+lon1);
        Log.e("destination: ",""+lat2+","+lon2);

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public void gotLocation(Location location) {
       currentLatLng = new LatLng(location.getLatitude(),location.getLongitude());
       mMap.clear();
        mMap.addMarker(new MarkerOptions().position(currentLatLng));
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(currentLatLng, 14.0f);
        mMap.animateCamera(yourLocation);
        SharedPreferences sharedPreferences = getSharedPreferences(MAP_SHARED,Context.MODE_PRIVATE);
        String lati = sharedPreferences.getString(LATITUDE,null);
        String longi = sharedPreferences.getString(LONGITUDE,null);
        if (lati != null && longi != null) {
            if (!lati.equalsIgnoreCase("" + currentLatLng.latitude) && !longi.equalsIgnoreCase("" + currentLatLng.longitude))
                textView.setText("Distance: " + distance(Double.parseDouble(lati), Double.parseDouble(longi), currentLatLng.latitude, currentLatLng.longitude) + "KM");
        else
                textView.setText("Distance: 0 KM");
        }else
            textView.setText("Distance: N/A");

    }

    private Timer timer;
    private TimerTask timerTask = new TimerTask() {

        @Override
        public void run() {

            Log.e("Working::","Timer.....");
            locationTracker =new LocationTracker(MainActivity.this,MainActivity.this);
            locationTracker.onUpdateLocation();
        }
    };

    public void start() {
        if(timer != null) {
            return;
        }
        timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, SCHEDULE_TIME);
    }

    public void stop() {
        timer.cancel();
        timer = null;
    }
}
